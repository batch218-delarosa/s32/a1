const http = require("http");
const port = 4000

const server = http.createServer((req, res) => {


	if(req.url == "/" && req.method == "GET") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Ends the response process
		res.end('Welcome to booking system');		
	}

	if(req.url == "/profile" && req.method == "GET") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Ends the response process
		res.end('Welcome to your profile');		
	}

	if(req.url == "/courses" && req.method == "GET") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Ends the response process
		res.end('Here\'s our courses available');		
	}

	if(req.url == "/addCourse" && req.method == "POST") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Ends the response process
		res.end('Add course to our resources');		
	}

	if(req.url == "/updateCourse" && req.method == "PUT") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Ends the response process
		res.end('Update a course to a database');		
	}

	if(req.url == "/archiveCourse" && req.method == "DELETE") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		// Ends the response process
		res.end('Archive courses to our resources');		
	}






});


server.listen(port);

console.log(`Listening on port ${port}`)


